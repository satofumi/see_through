import numpy as np
import cv2

capture = cv2.VideoCapture(6)
if not capture.isOpened():
    raise('IO Error')

while (True):
    ret, frame = capture.read()
    if not ret:
        continue

    cv2.imshow('frame', frame)
    key = cv2.waitKey(1) & 0xff
    if key == ord('q'):
        break

    elif key == ord('s'):
        cv2.imwrite('capture.png', frame)

capture.release()
cv2.destroyAllWindows()
