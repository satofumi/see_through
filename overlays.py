import cv2
import sys

src_file = sys.argv[1]
add_file = sys.argv[2]

src = cv2.imread(src_file)
add = cv2.imread(add_file)

alpha = 0.3

while (True):
    dest = cv2.addWeighted(src, 1 - alpha, add, alpha, 0)
    cv2.imshow('output', dest)
    key = cv2.waitKey(1) & 0xff
    if key == ord('q'):
        break

    elif key == ord('+'):
        alpha += 0.1

    elif key == ord('-'):
        alpha -= 0.1
