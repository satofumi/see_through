UNITY_EXE = /C/Program\ Files/Unity/Hub/Editor/2019.2.0a7/Editor/Unity.exe
VERSION = `cat ChangeLog.txt | awk "NR==2,NR==2 {print}" | awk '$$0 = substr($$0, 5)'`
PACKAGE_DIR = ScrapeImage-$(VERSION)

all :

clean :

.PHONY : all clean dist

dist :
	$(UNITY_EXE) -batchmode -nographics -projectPath $(PWD)/scrape_image -buildWindowsPlayer $(PWD)/ScrapeImage.exe -quit
	$(RM) -rf $(PACKAGE_DIR)
	mkdir -p $(PACKAGE_DIR)
	cp README.md $(PACKAGE_DIR)/README.txt
	cp UnityPlayer.dll LICENSE.txt ChangeLog.txt $(PACKAGE_DIR)/
	cp scrape_image/Assets/Data/COPYING.txt $(PACKAGE_DIR)/NyARToolkit_license.txt
	mv ScrapeImage.exe ScrapeImage_Data $(PACKAGE_DIR)
	cp -R MonoBleedingEdge $(PACKAGE_DIR)
	zip -r $(PACKAGE_DIR).zip $(PACKAGE_DIR)/
