# See Through

NyARToolkit を用いて撮影した画像を透過させる演出を提供する Unity プログラム


## ライセンス

 - MIT License




## 使い方

 - このリポジトリをクローンする。
 - Unity でプロジェクト scrape_image を開く。
    - NyARToolkit for Unity3D をダウンロードして Import する。
        - https://nyatla.jp/nyartoolkit/wp/?page_id=556

 - Resources/ フォルダに 000, 001, 002 と順にフォルダを作成し、透過前と透過後の画像を登録および NtfFileGenerator.jar で生成したデータを登録する。
    - Resources/000/0.png ... 透過前の画像（認識させたい画像）
    - Resources/000/1.png ... 透過後の画像（置き換えたい画像）
    - Resources/000/d.bytes ... NtfFileGenerator.jar で生成したデータファイル。
        - もっと画像を登録する場合はフォルダを 001, 002 と順にフォルダ作成して 000 フォルダと同様にファイルを登録する。
 - Unity でビルドして PC で実行する。
    - USB カメラが接続されていれば PC で動作確認できる。

 - Android に転送して実行する。
 - アプリをリリースする場合は、このプロジェクトで利用している NyARToolkit が LGPL ライセンスであることと、このプロジェクトが MIT ライセンスであることに留意して対応してください。


## Author

 - satofumi
    - twitter: @satofumi_


## 素材

### 効果音素材
 - DOVA-SYNDROME
    - https://dova-s.jp/

### イラスト素材
 - いらすとや
    - https://www.irasutoya.com/
