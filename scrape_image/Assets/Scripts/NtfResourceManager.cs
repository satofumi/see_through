﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using NyARUnityUtils;

public class NtfResourceManager : MonoBehaviour
{
    public Shader DiffuseShader;
    public Shader TransparentShader;
    public GameObject ResourcePrefab;


    public List<NtfResource> Load(NyARUnityNftSystem ntfSystem)
    {
        var resources = new List<NtfResource>();
        for (int i = 0; i < 999; ++i)
        {
            var resource = Instantiate(ResourcePrefab).GetComponent<NtfResource>();

            var folder = $"{i:000}";

            try
            {
                var originalTexture = Resources.Load(Path.Combine(folder, "0")) as Texture;
                var transparentTexture = Resources.Load(Path.Combine(folder, "1")) as Texture;
                var data = new MemoryStream(((TextAsset)Resources.Load(Path.Combine(folder, "d"), typeof(TextAsset))).bytes);

                var originalRenderer = resource.Original.GetComponent<Renderer>();
                originalRenderer.material = new Material(DiffuseShader);
                SetMaterial(originalRenderer, originalTexture, 1);

                var transparentRenderer = resource.Transparent.GetComponent<Renderer>();
                transparentRenderer.material = new Material(TransparentShader);
                SetMaterial(transparentRenderer, transparentTexture, 0);

                resource.MarkerId = ntfSystem.addNftTarget(data, 100);
                SetTransform(resource.Objects, originalTexture);
            }
            catch (System.Exception)
            {
                return resources;
            }

            resource.Initialize();
            resources.Add(resource);
        }
        return resources;
    }

    private void SetMaterial(Renderer renderer, Texture texture, float alpha)
    {
        renderer.material.mainTexture = texture;
        renderer.material.color = new Color(1, 1, 1, alpha);
    }

    private void SetTransform(GameObject objects, Texture texture)
    {
        float height = 100.0f * texture.height / texture.width;
        objects.transform.localScale = new Vector3(100, height, 1);
        objects.transform.localPosition = new Vector3(-50, height / 2, 0);
    }
}
