﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NyARUnityUtils;

public class NtfResource : MonoBehaviour
{
    public GameObject Objects;
    public GameObject Original;
    public GameObject Transparent;
    public int MarkerId { get; set; }

    public enum VisibleState
    {
        Hide,
        Show,
        Scope,
    }

    private Material originalMaterial;
    private Material transparentMaterial;


    public void Initialize()
    {
        originalMaterial = Original.GetComponent<Renderer>().material;
        transparentMaterial = Transparent.GetComponent<Renderer>().material;
    }

    public void SetVisible(VisibleState visible, NyARUnityNftSystem ntfSystem)
    {
        switch (visible)
        {
            case VisibleState.Hide:
                transform.localPosition = new Vector3(0, 0, 0);
                break;

            case VisibleState.Show:
                Original.SetActive(true);
                Transparent.SetActive(true);
                ntfSystem.setTransform(MarkerId, transform);
                break;

            case VisibleState.Scope:
                ntfSystem.setTransform(MarkerId, transform);
                Original.SetActive(false);
                Transparent.SetActive(false);
                break;
        }
    }

    private void SetAlpha(Material material, float alpha)
    {
        material.color = new Color(1, 1, 1, alpha);
    }

    public float Alpha
    {
        set
        {
            float alpha = Mathf.Clamp(value, 0, 1.0f);
            SetAlpha(originalMaterial, 1.0f - alpha);
            SetAlpha(transparentMaterial, alpha);
        }
    }
}
