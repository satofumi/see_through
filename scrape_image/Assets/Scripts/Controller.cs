﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using NyARUnityUtils;
using jp.nyatla.nyartoolkit.cs.markersystem;


public class Controller : MonoBehaviour
{
    public GameObject BackgroundPanel;
    public GameObject CapturedBackground;
    public Shader TextureShader;
    public NtfResourceManager ResourceManager;
    public Button CaptureButton;
    public Button CloseButton;
    public Image FlushEffectPanel;
    public AudioSource ShutterAudio;

    private NyARUnityNftSystem ntfSystem;
    private NyARUnityWebCam webCam;
    private List<NtfResource> ntfResources;

    private enum ControlState
    {
        Searching,
        Captured,
    }
    private ControlState controlState;
    private bool detected;
    private HashSet<NtfResource> detectedResources = new HashSet<NtfResource>();
    private float imageAlpha;
    private Vector3 lastTouchedPosition;


    private void Awake()
    {
        var devices = WebCamTexture.devices;
        if (devices.Length <= 0)
        {
            Debug.LogError("No Webcam.");
            return;
        }

        var w = new WebCamTexture(600, 480, 15);
        webCam = NyARUnityWebCam.createInstance(w);
        var config = new NyARNftSystemConfig(new MemoryStream(((TextAsset)Resources.Load("camera_para5", typeof(TextAsset))).bytes), webCam.width, webCam.height);

        ntfSystem = new NyARUnityNftSystem(config);
        ntfResources = ResourceManager.Load(ntfSystem);

        BackgroundPanel.GetComponent<Renderer>().material.mainTexture = w;
        ntfSystem.setARBackgroundTransform(BackgroundPanel.transform);

        CapturedBackground.transform.localPosition = BackgroundPanel.transform.localPosition;
        CapturedBackground.transform.localRotation = BackgroundPanel.transform.localRotation;
        CapturedBackground.transform.localScale = BackgroundPanel.transform.localScale;
        CapturedBackground.GetComponent<Renderer>().material = new Material(TextureShader);
        CapturedBackground.SetActive(false);

        ntfSystem.setARCameraProjection(Camera.main);
    }

    private void Start()
    {
        controlState = ControlState.Searching;
        CloseButton.gameObject.SetActive(false);
        webCam.start();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        switch (controlState)
        {
            case ControlState.Searching:
                webCam.update();
                ntfSystem.update(webCam);
                imageAlpha = 0;

                detected = false;
                detectedResources.Clear();
                foreach (var resource in ntfResources)
                {
                    bool exists = ntfSystem.isExist(resource.MarkerId);
                    var visible = NtfResource.VisibleState.Hide;
                    if (exists)
                    {
                        detected = true;
                        detectedResources.Add(resource);
                        visible = NtfResource.VisibleState.Scope;
                    }
                    resource.SetVisible(visible, ntfSystem);
                }
                break;

            case ControlState.Captured:
                imageAlpha = Mathf.Clamp(imageAlpha + SwipedAmount(), 0, 1.0f);
                foreach (var resource in ntfResources)
                {
                    resource.Alpha = imageAlpha;
                }
                break;
        }
    }

    private void OnDestroy()
    {
        ntfSystem.shutdown();
    }

    private float SwipedAmount()
    {
        float amount = -0.01f;

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            lastTouchedPosition = Input.mousePosition;
        }
        else if (Input.GetKey(KeyCode.Mouse0))
        {
            if (Input.mousePosition != lastTouchedPosition)
            {
                amount = (Input.mousePosition - lastTouchedPosition).magnitude / 10000.0f;
                lastTouchedPosition = Input.mousePosition;
            }
        }

        return amount;
    }

    public void CaptureButtonPressed()
    {
        if (!detected)
        {
            return;
        }

        ShutterAudio.Play();
        StartCoroutine(FlushEffect());

        controlState = ControlState.Captured;
        CaptureButton.interactable = false;
    }

    public void CloseButtonPressed()
    {
        controlState = ControlState.Searching;
        CloseButton.gameObject.SetActive(false);
        CaptureButton.interactable = true;
        foreach (var resource in ntfResources)
        {
            resource.SetVisible(NtfResource.VisibleState.Hide, ntfSystem);
        }
        CapturedBackground.SetActive(false);
        BackgroundPanel.SetActive(true);
    }   

    private IEnumerator FlushEffect()
    {
        yield return new WaitForEndOfFrame();
        var capturedTexture = new Texture2D(Screen.width, Screen.height);
        capturedTexture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        capturedTexture.Apply();
        CapturedBackground.GetComponent<Renderer>().material.mainTexture = capturedTexture;
        CapturedBackground.SetActive(true);

        BackgroundPanel.SetActive(false);
        CloseButton.gameObject.SetActive(true);

        FlushEffectPanel.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.4f);

        foreach (var resource in detectedResources)
        {
            resource.SetVisible(NtfResource.VisibleState.Show, ntfSystem);
        }

        var image = FlushEffectPanel.GetComponent<Image>();
        float alpha = 1.0f;
        while (alpha > 0)
        {
            alpha -= 0.1f;
            image.color = new Color(1, 1, 1, Mathf.Max(alpha, 0));
            yield return new WaitForSeconds(0.05f);
        }

        image.color = new Color(1, 1, 1, 1);
        FlushEffectPanel.gameObject.SetActive(false);
    }
}
