import numpy as np
import cv2
from matplotlib import pyplot as plt
import sys


file1 = sys.argv[1]
file2 = sys.argv[2]

img1 = cv2.imread(file1, 0)
img2 = cv2.imread(file2, 0)

akaze = cv2.AKAZE_create()

kp1, dest1 = akaze.detectAndCompute(img1, None)
kp2, dest2 = akaze.detectAndCompute(img2, None)

bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)

matches = bf.match(dest1, dest2)
matches = sorted(matches, key = lambda x:x.distance)

img3 = cv2.drawMatches(img1, kp1, img2, kp2, matches[:40], None, flags=2)

plt.imshow(img3), plt.show()
